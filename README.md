# Birthday alert

Bash + Python3 script that displays people's birthdays.

## Usage

Create a `list.txt` file next to the script files.

This file should contain one date and name per line, in the following format: `dd/mm name`, like `31/01 John`.

You can add comments using `#` as the first character.

## Automation

To run it every 5 hours as a cronjob, you can add the following to your crontab:

`0 */5 * * * XDG_RUNTIME_DIR=/run/user/$(id -u) sh SCRIPT_PATH`

(the `XDG_RUNTIME_DIR=/run/user/$(id -u)` part is from [this answer](https://stackoverflow.com/a/53598510/13302352))

## How it works

### Bash

The bash scripts passes the output of the python script to `notify-send`.

### Python3

The scripts iterates in the list file to print relevant names.

