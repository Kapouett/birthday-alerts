#!/usr/bin/env python3
# -*- coding: utf-8 *-*

# By Kapouett

# SPDX-License-Identifier: WTFPL
# This work is free. You can redistribute it and/or modify it under the
# terms of the Do What The Fuck You Want To Public License, Version 2,
# as published by Sam Hocevar. See the LICENSE file for more details.

import datetime

if __name__ == '__main__':
	time = datetime.datetime.now()
	day = time.strftime('%d')
	month = time.strftime('%m')
	
	with open('list.txt', 'r') as file:
		for line in file:
			if line.startswith('#'):
				continue
			slash = line.find('/')
			space = line.find(' ')
			if slash < 0 or space < 0:
				print('Malformed line: '+line)
				break
			
			if line[0:slash] == day and line[slash+1:space] == month:
				print(line[space+1:len(line)])

