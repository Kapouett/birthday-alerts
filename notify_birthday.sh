#!/bin/bash

# SPDX-License-Identifier: WTFPL
# This work is free. You can redistribute it and/or modify it under the
# terms of the Do What The Fuck You Want To Public License, Version 2,
# as published by Sam Hocevar. See the LICENSE file for more details.

cd "${0%/*}"

notif_text=`python3 ./print_todays_birthdays.py`
if [ ! -z "$notif_text" ]; then
	notify-send "🎂 Birthday!" "$notif_text"
fi

